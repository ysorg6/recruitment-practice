package com.dq.springboot_recruit.mapper;

import java.util.List;

import com.dq.springboot_recruit.entity.CompanyInfo;
import com.dq.springboot_recruit.entity.DeliveryInfo;
import com.dq.springboot_recruit.entity.ResumeInfo;


public interface IDeliveryInfoMapper {
	//查找简历编号
	ResumeInfo findr(ResumeInfo resumeInfo);
	//查找企业名称
	CompanyInfo findc(CompanyInfo companyInfo);
	//添加简历
	int add(DeliveryInfo deliveryInfo);
	//查找投递记录信息
	List<DeliveryInfo> findd(DeliveryInfo deliveryInfo);
	//查找收到的简历
	List<DeliveryInfo> findReceived(DeliveryInfo deliveryInfo);
	//批准简历
	int approved(DeliveryInfo deliveryInfo);
	//拒绝简历
	int refuse(DeliveryInfo deliveryInfo);
}
